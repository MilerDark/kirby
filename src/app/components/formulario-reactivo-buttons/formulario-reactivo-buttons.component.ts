import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-formulario-reactivo-buttons',
  templateUrl: './formulario-reactivo-buttons.component.html',
  styleUrls: ['./formulario-reactivo-buttons.component.css']
})
export class FormularioReactivoButtonsComponent implements OnInit {
  form!: FormGroup;
  list!: string;
  checkbox: boolean = false;
  constructor(private fb: FormBuilder) { 
    this.accionBotones();
  }

get botones(){
  return this.form.get('botones') as FormArray;
}

  ngOnInit(): void {
  }

 
  accionBotones(): void{
    this.form = this.fb.group(
  {
    id:[''],
    botones: this.fb.array([[this.form = this.fb.group({
  })
  ]])
  
    
  });

  }

  agregar():void{
    this.botones.push(this.fb.control(''));
  }

  borrar(i:number):void{
    this.botones.removeAt(i);
  }

  guardar():void{
    this.limpiar();
     
    
  }

  limpiar():void{
    this.form.reset({
      botones:''
    })
    this.list = '';

  }

  eliminarTodo():void{
    this.form = this.fb.group(
      {
        botones: this.fb.array([])
      }
    )
  }
   
} 


